
import java.rmi.*;
import java.rmi.server.*;
import java.io.*;
import java.net.*;

public class TemperatureServerImpl extends UnicastRemoteObject //permite ao objeto receber chamadas remotas
    implements TemperatureServer {


    public TemperatureServerImpl() throws RemoteException {
	super();

	try {
	    System.out.println(metodoServidor());
	} catch (RemoteException e){
	    System.out.println("Excecao no servidor "+e.getMessage());
	    e.printStackTrace();
	}
    }//fim construtor

    public String metodoServidor() throws RemoteException {
	return "Recebi mensagem do servidor.";
    }
    
    public String metodo2() throws RemoteException {
	return "Outra mensagem do servidor.";
    }

    public static void main(String [] args) throws Exception {
	System.err.println("Inicializando o servidor...");	

	//Vincula (bind) o objeto de TemperatureServerImpl ao rmiregistry,
	//e atribui o nome //localhost/TempServer ao objeto remoto, ou seja,
	//o nome utilizado pelo cliente para referenciar o objeto remoto no servidor

	String serverObjectName = "//localhost/TempServer";
	Naming.rebind( serverObjectName, new TemperatureServerImpl() );
	
	System.err.println("Servidor inicializado.");	
    }

}//fim classe